#
# Utilitaires
#

import time


def chronometrer(fonction):

    def decorateur(*args, **kwargs):
        temps_debut = time.clock()
        resultat = fonction(*args, **kwargs)
        temps_fin = time.clock()
        # print('Durée :', temps_fin - temps_debut, 'secondes')
        duree = temps_fin - temps_debut
        print('Durée : {:.6f} secondes'.format(duree))
        return resultat

    return decorateur
