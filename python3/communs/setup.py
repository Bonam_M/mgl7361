#
# Fichier setup
#

from setuptools import setup


setup(
    name='communs',
    version='1.0',
    description='Classes et fonctions communes',
    author='Louis Martin',
    author_email='louis.martin@uqam.ca',
    url='https://bitbucket.org/louis_martin/mgl7361',
    license='MIT',
    packages=['communs'],
)
