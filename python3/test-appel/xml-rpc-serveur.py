#
# Serveur RPC
#

from xmlrpc.server import SimpleXMLRPCServer
from xmlrpc.server import SimpleXMLRPCRequestHandler

from bidule import Bidule


def main():
    try:
        bidule = Bidule()
        serveur = creerServeur()
        serveur.register_instance(bidule)
        print("Lancement du serveur, Ctrl-C pour demander l'arrêt.")
        serveur.serve_forever()
    except KeyboardInterrupt:
        print("\nArrêt demandé. Fin de l'exécution.")


def creerServeur():
    # Limiter les requêtes à un path seulement
    class RequestHandler(SimpleXMLRPCRequestHandler):
        rpc_paths = ('/RPC2',)

    serveur = SimpleXMLRPCServer(
        ('127.0.0.1', 8080),
        requestHandler=RequestHandler,
        logRequests=False,
        allow_none=True
    )
    # serveur.register_introspection_functions()
    return serveur


if __name__ == '__main__':
    main()
