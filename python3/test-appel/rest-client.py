#
# Client REST
#

import requests

from communs import chronometrer


def main():
    n = 1000
    url = 'http://127.0.0.1:8080/bidule'
    resultat = executer(url, n)
    print('Valeur du résultat à la fin :', resultat)


@chronometrer
def executer(url, n):
    incrementeur = url + '/incrementeur'
    for i in range(n):
        requests.post(incrementeur)
    return requests.get(url).json()


if __name__ == '__main__':
    main()
