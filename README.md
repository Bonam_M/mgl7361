# LISEZ-MOI

Dépôt GIT relatif au cours MGL7361 de l'automne 2017.

## Pour télécharger le dépôt (format ZIP)

<https://bitbucket.org/louis_martin/mgl7361/downloads/>

## Pour cloner le dépôt

La manière la plus facile est :

1. d'installer le client GIT gratuit SourceTree <https://www.sourcetreeapp.com/> ;

1. d'utiliser le protocole HTTPS.

----

# Section relative à Python 3

## Préalable

Installation de Python 3 <https://www.python.org/>.

## Commandes pratiques pour l'environnement virtuel de Python 3

**Important** : au préalable se positionner dans le répertoire `mgl7361/python3`.

### Initialiser l'environnement

Pour initialiser le répertoire `ENV` :

    $ python3 -m venv ENV

**Note** Ne faire cette commande qu'une seule fois.

### Activer et déactiver l'environnement

Pour activer, lancer la commande :

    $ source ENV/bin/activate

Pour désactiver :

    $ deactivate

### Installer ou mettre à jour les bibliothèques de code

Lancer la commande :

    $ pip3 install -r requirements.txt -U

### Installer ou mettre à jour le package `communs`

Se positionner dans le répertoire `mgl7361/python3/communs`

Lancer la commande :

    $ pip3 install . -U

Ou avec un lien symbolique permettant de refléter immédiatement les changements au code source, lancer la commande :

    $ pip3 install -e .

----

# Section relative à Pandoc

## Préalable

Installation de LaTeX :

- pour Mac OS <http://www.tug.org/mactex/>

- pour Windows <https://miktex.org/>

Installation de Pandoc <https://pandoc.org/>.

## Exemple

Un exemple est inclus dans le répertoire `pandoc`.

- L'intrant est le fichier `exemple.md` en _markdown_.

- L'extrant est le fichier `exemple.pdf`.

Pour lancer la transformation, se positionner dans le répertoire `mgl7361/pandoc` et faire la commande suivante :

    ./pdf.sh exemple.md

----
